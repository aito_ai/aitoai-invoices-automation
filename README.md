# aitoai-invoices-automation

This repository contains the dataset and show example of using aito to do invoice automation. 

The data in the example is purely fictional,and has been created for demonstration purposes only.

Please feel free to comment the code and send us pull requests or issues, if you find problems with the code.

We are setting up an interactive environment where you could spawn your own instance to try out these examples. If you are interested
please follow [the instructions on getting in touch with us in the documentation](https://bitbucket.org/aito_ai/aitoai-documentation#markdown-header-11-feedback-reporting-bugs-or-inconsistencies).

This code is licensed under the MIT license, and you are free to copy it, modify it and distribute it as you see fit. 



## Getting Started
The project contains 2 directories:
```
* data: Contains generated data in csv format (csv/) or json format (/json)
* queries: Contains the queries or queries template used for invoice automation
```

## Data
### Dataset: 
1. Products ([csv](data/csv/products.csv) or [json](data/json/products.json))
2. Customers ([csv](data/csv/customers.csv) or [json](data/json/customers.json))
3. Orders ([csv](data/csv/orders.csv) or [json](data/json/orders.json))
4. Invoices ([csv](data/csv/invoices.csv) or [json](data/json/invoices.json)) 

More information about the [dataset](data)

## Queries
1. predict_*_from_order_info: query template for predicting a feature in invoices using order information (API:PREDICT)
2. find_info_from_order: query session for finding information given order_id (API:FIND)