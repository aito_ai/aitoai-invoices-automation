# Invoices Automation Dataset 

## Structure

1. Data files in [csv format](csv) containing tables' content
2. Data files in [json format](json) containing tables' content, schema, and the products table splitted to smaller size

## Data generation rules: 

These are the rules that was used to generate the dataset

1. Products:
	- Products is taken from jcpenny product data from kaggle. 
	- Removed products that does not have sku or name or price
	- Fixed rows that information was placed wrongly from the jcpenny datafile (price was in category, etc.)
	- Removed column list_price

2. Customers:
	- Generated data
	- Limit number of unique cities to 50, and number of unique languages to 10

3. Orders:
	- Generated based on customers and products data
	- Each customer has a budget minimum and budget maximum (randomize). Currently has 3 ranges:min = [100, 500, 1500] max = [200, 2000, 10000]. Customer budget is generated with probabilty = [0.5, 0.45, 0.05] for each corresponding budget type
	- Customer also have a buying pattern for category of products. A customer will have 5-10 main categories (30 categories in products that have the highest sum of product rice) and 1-3 sub categories. The customer will choose randomly product and amount in categories so that 70% time the customer would choose main categories and 30% time would choose sub. The products will be chosen so that the customer would roughly stay in his/her budget
	- TODO: 
		Buying behavior throughout time
		60% buy products with high rating (>3)

4. Invoices:
	- Use orders -> invoices
	- Generate distributor: 
		+ Each unique city will or will not have a designated distributor. 
		+ Neighbour cities: A city can have 2 - 4 neighbors.
		+ Invoice distributor is based on customer's location (city): designated distributor of the city or find designated distributor of neighbor cities (can be neighbor of neighbor)
	- Generate warehouse:
		+ If quantity > 10 for all item => "Big warehouse"
		+ Else warehouse is randomized based on price range [<10, <50, <200, >200]
	- Generate sales rep:
		+ if customers have bought more than > 1500 => special sales rep
		+ sales rep for each price range of total order amount [<100, < 500, < 1000]
		+ After 03/2015, sales rep for < 1000 is replaced with a newly hired sales rep

