import requests
import json
from pprint import pprint


def send_request(env_url, api_key, json_string):
	# Create request
	headers = {}
	headers['Content-Type'] = "application/json; charset=utf-8"
	headers['x-api-key'] = api_key

	url = env_url + "/api/schema/_mapping"

	# print('url: ', url)
	# print('data: ', json_string)

	request = requests.put(url, data = json_string, headers = headers)
	if request.status_code != 200:
		raise ValueError('Something is wrong: ', request.text)

	request_result = json.loads(request.text)
	print('Load schema successful ')
	pprint(request_result)
