def env_url_check(env_url):
	if (".api.aito.ai" not in env_url) and (".api.aito.ninja" not in env_url):
		raise ValueError("Incorrect env_url. The env_url should be in form of <name>.api.aito.ai")

def api_key_check(api_key):
	return True

def path_check(path):
	if not (path.endswith('.csv') or path.endswith('.json')):
		raise ValueError("File must be csv or json")
