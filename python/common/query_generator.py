"""
This is a library for generating query content for aito
"""


def generate_data_batch_query(table_name, content):
	"""Summary
	
	Args:
	    table_name (TYPE): Description
	    content (TYPE): Description
	
	Returns:
	    TYPE: Description
	"""
	return query



def generate_predict_query(table_name, conditions, predict_feature, exclusiveness):
	"""Summary
	This function generate a predict query based on given information
	Args:
	    table_name (String): name of table to be looked for to put in context
	    conditions (dict of dict of tuple): list of conditions with keys is the condition ("when, where, etc.")
	    									in conditions[key] is a dict with key is the feature and the value is a tuple of (clause phrase, type, value)
	    									e.g: conditions["when"] = {}
	    										conditions["when"]["A"] = ("is", Int, 15) => {"when": "A", "is":15}
	    										conditions["when"]["B"] = ("has", String, "yes") => {"when": "B", "has":"yes"}
	    predict_feature (String): name of feature to be predicted
	
	Returns:
	    query: String
	"""
	query = ""
	query += "{"
	query += "\"context\":"
	query += "\"" + table_name + "\","
	query += "\"q\" : ["

	for condition in conditions:
		condition_queries = ""

		condition_features = list(conditions[condition].keys())

		for i in range(len(condition_features)):
			condition_feature = condition_features[i]

			clause = conditions[condition][condition_feature][0]
			value_type = conditions[condition][condition_feature][1]
			value = conditions[condition][condition_feature][2]

			condition_queries += "{\"" + condition + "\" :" + "\"" + condition_feature + "\","
			condition_queries += "\"" + clause + "\":"
			
			if value_type == "String":
				condition_queries += "\"" + value + "\""
			if (value_type == "Int") or (value_type == "Decimal"):
				condition_queries += str(value)

			condition_queries += "}"

			if i != (len(condition_features) - 1):
				condition_queries += ","

		query += condition_queries

	query += "],"

	if exclusiveness == "false":
		query += "\"exclusive\": false,"

	query += "\"predict\":" + "\"" + predict_feature + "\""
	query += "}"
	
	return(query)


def generate_simple_whereHit_query(table_name, conditions):
	"""Summary
	This function generate a predict query based on given information
	Args:
	    table_name (String): name of table to be looked for to put in context
	    conditions (dict of dict of tuple): list of conditions with keys are the feature and the value is a tuple of (clause phrase, type, value)
	    									e.g:conditions["A"] = ("is", Int, 15) => {"whereHit": "A", "is":15}
	    										conditions["B"] = ("has", String, "yes") => {"whereHit": "B", "has":"yes"}	
	Returns:
	    query: String
	"""
	query = ""
	query += "{"
	query += "\"context\":"
	query += "\"" + table_name + "\","
	query += "\"q\" : ["
	query += "{\"find\":{}},"

	condition_queries = ""

	conditions_features = list(conditions.keys())
	for i in range(len(conditions_features)):

		conditions_feature = conditions_features[i]

		clause = conditions[conditions_feature][0]
		value_type = conditions[conditions_feature][1]
		value = conditions[conditions_feature][2]

		condition_queries += "{\"whereHit\" :" + "\"" + conditions_feature + "\","
		condition_queries += "\"" + clause + "\":"
			
		if value_type == "String":
			condition_queries += "\"" + value + "\""
		if (value_type == "Int") or (value_type == "Decimal"):
			condition_queries += str(value)

		condition_queries += "}"

		if i != (len(conditions_features) - 1):
			condition_queries += ","

	query += condition_queries
	query += ']}'

	return(query)
