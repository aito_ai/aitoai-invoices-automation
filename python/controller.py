from common import *
from api_schema import *
from api_data import *
from invoices_automation import *
from common.content_loader import load_from_file_to_json
from pprint import pprint

def config_data(env_url, api_key):
	print('###############################')
	print('Deleting existing schema in env')
	delete_schema.send_request(env_url, api_key)

	print('###############################')
	print('Populating schema')
	schema_path = "../data/json/schema.json"
	load_schema.send_request(env_url, api_key, load_from_file_to_json(schema_path))

	print('###############################')
	print('Populating customers table')
	customers_path = "../data/json/customers.json"
	load_table_content.send_request(env_url, api_key, 'customers', load_from_file_to_json(customers_path))

	print('###############################')
	print('Populating products table')
	products_paths = ["../data/json/products_splited_0.json", "../data/json/products_splited_1.json", "../data/json/products_splited_2.json"]
	for products_path in products_paths:
		load_table_content.send_request(env_url, api_key, 'products', load_from_file_to_json(products_path))

	print('###############################')
	print('Populating orders table')
	orders_path = "../data/json/orders.json"
	load_table_content.send_request(env_url, api_key, 'orders', load_from_file_to_json(orders_path))

	print('###############################')
	print('Populating invoices table')
	invoices_path = "../data/json/invoices.json"
	load_table_content.send_request(env_url, api_key, 'invoices', load_from_file_to_json(invoices_path))


def main():
	print('Welcome to aito python library')
	env_url = input('Please input your environement url: ')
	error_handler.env_url_check(env_url)
	api_key = input('Please input your api key: ')
	error_handler.api_key_check(api_key)

	config_data(env_url, api_key)

	# command_list = ['invoice automation by order id', 'invoice automation by file containing order id', 'exit']
	command_list = ['invoice automation by order id', 'exit']

	print('###############################')
	print('Command list: ')
	for i in range(len(command_list)):
		print('[' + str(i) + ']' + ': ' + command_list[i])
	
	while True:
		try:
			command = int(input('Choose command number: '))
			if command not in range(len(command_list)):
				raise Exception('Wrong command number. Please choose a number from range: ', str(0), '-', str(len(command_list) - 1))
		except Exception as error:
			print('Caught an error: ', error)

		if command == 1:
			print('Goodbye')
			exit()

		if command == 0:
			order_id = input('Input order id: ')

			order_info, predicted_results = invoices_automation.invoices_automation(env_url, api_key, order_id)

			if len(order_info) == 0:
				print('Cannot find the given order id')

			else:
				for i in range(len(order_info)):
					row_order_info = order_info[i]
					row_predicted_result = predicted_results[i]
					print('Order ID: ', row_order_info['id'], ' Customer ID: ', row_order_info['customer_id'], ' Product ID:', row_order_info['product_id'], 
						' Product_quantity: ', row_order_info['quantity'], ' time: ', row_order_info['time'])
					print('Predicted')
					print(' sales rep: ', row_predicted_result['sales_rep'], ' warehouse: ', row_predicted_result['warehouse'], 'distributor: ', row_predicted_result['distributor'])

		# if command == 1:
		# 	path == input('Input path to file: ')
			

main()