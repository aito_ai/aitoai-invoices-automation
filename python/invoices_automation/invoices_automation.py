import os
import sys
import pandas as pd
import numpy as np
import requests
import json
from pprint import pprint
sys.path.append(os.path.join(os.path.dirname(__file__),'../'))
from common.query_generator import *


def get_info_from_order_id(env_url, api_key, order_id):
	headers = {}
	headers['Content-Type'] = "application/json; charset=utf-8"
	headers['x-api-key'] = api_key

	conditions = {}
	conditions['id'] = ('is', 'String', order_id)

	query = generate_simple_whereHit_query('orders', conditions)

	# print('query ', query)

	request = requests.post(env_url + '/api/_find', data = query, headers = headers)
	if request.status_code != 200:
			raise ValueError('Something is wrong', request.reason)

	request_results = json.loads(request.text)

	results = []

	for request_result in request_results['rows']:
		results.append(request_result['row'])

	return results

def predict_from_order_info(env_url, api_key, order_info):
	columns = ['order_id', 'customer_id', 'product_id', 'product_quantity', 'time']
	headers = {}
	headers['Content-Type'] = "application/json; charset=utf-8"
	headers['x-api-key'] = api_key

	results = []

	for row in order_info:
		conditions = {}
		conditions['when'] = {}
		conditions['when']['order_id'] = ("is", "String", row['id'])
		conditions['when']['customer_id'] = ("is", "String", row['customer_id'])
		conditions['when']['product_id'] = ("is", "String", row['product_id'])
		conditions['when']['product_quantity'] = ("is", "Int", row['quantity'])
		conditions['when']['time'] = ("is", "String", row['time'])

		sales_rep_query = generate_predict_query('invoices', conditions, 'sales_rep', 'true')
		request = requests.post(env_url + "/api/_predict", data = sales_rep_query, headers = headers)
		if request.status_code != 200:
			raise ValueError('Something is wrong', request.reason)
		request_result = json.loads(request.text)
		sales_rep = request_result[0]['variable']['feature']

		warehouse_query = generate_predict_query('invoices', conditions, 'warehouse', 'true')
		request = requests.post(env_url + "/api/_predict", data = warehouse_query, headers = headers)
		if request.status_code != 200:
			raise ValueError('Something is wrong', request.reason)
		request_result = json.loads(request.text)
		warehouse = request_result[0]['variable']['feature']

		distributor_query = generate_predict_query('invoices', conditions, 'distributor', 'true')
		request = requests.post(env_url + "/api/_predict", data = distributor_query, headers = headers)
		if request.status_code != 200:
			raise ValueError('Something is wrong', request.reason)
		request_result = json.loads(request.text)
		distributor = request_result[0]['variable']['feature']
		
		results.append({'sales_rep': sales_rep, 'warehouse': warehouse, 'distributor': distributor})
		
	return results



def invoices_automation(env_url, api_key, order_id):
	order_info = get_info_from_order_id(env_url, api_key, order_id)
	predict_result =  predict_from_order_info(env_url, api_key, order_info)
	return(order_info, predict_result)

